from flask import Flask,abort
from flask import render_template,request
import bot.api as botline

app = Flask(__name__)
cl = botline.Init()

@app.route('/')
def index():
    #botline.LoopForGetMsg(cl)
    response = '<h1>Hello World!</h1>'
    return render_template('index.html')

@app.route('/hello')
def hello():
    return '<h1>Hello World!</h1>'

@app.route('/line', methods=['GET', 'POST'])
def line_msg():
    cl = botline.Init()
    if request.method == 'POST':
        msg_send = request.form.get('msgs')
        group_selected = int(request.form.get('groups'))
        botline.send_the_msg(cl,'group', group_selected, msg_send)
        #cl.groups[0].sendMessage(msg_send)
        return '''<h1>Your message %s has sent!</h1>''' %msg_send
    else:
        #show_the_login_form()
        #cl = botline.Init()
        #cl = Init()
        enum = enumerate(cl.groups)
        msg = botline.do_the_work(cl,'group',1)
        return render_template('line.html', **locals())
        #return render_template('line.html', client = msg)

@app.route('/line/addContact', methods=['GET', 'POST'])
def line_add():
    cl = botline.Init()
    if request.method == 'POST':
        userid = request.form.get('userid')
        #group_selected = int(request.form.get('groups'))
        botline.add_by_userid(cl, userid)
        #cl.groups[0].sendMessage(msg_send)
        return '''<h1>User %s has added!</h1>''' %userid
    else:
        #show_the_login_form()
        #cl = botline.Init()
        #cl = Init()
        enum = enumerate(cl.groups)
        msg = botline.do_the_work(cl,'group',1)
        return render_template('line.html', **locals())
        #return render_template('line.html', client = msg)

@app.route('/user/<id>')
def get_user(id):
    user = load_user(id)
    if not user:
        abort(404)
    return '<h1>Hello, %s</h1>' % user.name


if __name__ == '__main__':
    app.run(debug=True)
